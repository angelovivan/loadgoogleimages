package com.example.loadgoogleimages;

import android.util.Log;

import java.io.IOException;

import okhttp3.*;

public class LoggingInterceptor implements Interceptor {
    private static final String TAG = LoggingInterceptor.class.getName();
    @Override
    public okhttp3.Response intercept(Chain chain) throws IOException {
        final Request request = chain.request();

        final long t1 = System.nanoTime();
        Log.v(TAG, String.format("Sending request %s with body: %s on %s%n%s", request.url(), request.body(), chain.connection(), request.headers()));

        final okhttp3.Response response = chain.proceed(request);

        final long t2 = System.nanoTime();
        Log.v(TAG, String.format("Received response for %s in %.1fms%n%s\nresponse.body()= \nresponse.code()=", response.request().url(), (t2 - t1) / 1e6d, response.headers(), response.body(), response.code()));

        return response;
    }
}
