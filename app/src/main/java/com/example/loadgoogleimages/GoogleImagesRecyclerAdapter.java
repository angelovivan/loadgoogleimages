package com.example.loadgoogleimages;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class GoogleImagesRecyclerAdapter extends RecyclerView.Adapter<GoogleImagesRecyclerAdapter.ViewHolder> {

    List<GoogleImageDataModel> mValues;
    Context mContext;
    protected ItemListener mListener;

    public GoogleImagesRecyclerAdapter(Context context, List<GoogleImageDataModel> values, ItemListener itemListener) {

        mValues = values;
        mContext = context;
        mListener = itemListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView imageView;
        public RelativeLayout relativeLayout;
        GoogleImageDataModel item;

        public ViewHolder(View v) {

            super(v);

            imageView = v.findViewById(R.id.imageView);
            relativeLayout = v.findViewById(R.id.relativeLayout);

        }

        public void setData(GoogleImageDataModel item) {
            this.item = item;

            Picasso.get().load(item.original).into(imageView);
            relativeLayout.setBackgroundColor((Color.GRAY));
        }


        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.onItemClick(item);
            }
        }
    }

    @Override
    public GoogleImagesRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.image_item_layout, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GoogleImagesRecyclerAdapter.ViewHolder holder, int position) {
        Picasso.get().load(mValues.get(position).original).into(holder.imageView);
        holder.relativeLayout.setBackgroundColor((Color.GRAY));
    }

    @Override
    public int getItemCount() {

        return mValues.size();
    }

    public interface ItemListener {
        void onItemClick(GoogleImageDataModel item);
    }
}

