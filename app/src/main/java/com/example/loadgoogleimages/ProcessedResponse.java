package com.example.loadgoogleimages;

import android.util.Log;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import okhttp3.ResponseBody;

@JsonIgnoreProperties
public abstract class ProcessedResponse {
    private static final String TAG = ProcessedResponse.class.getName();

    public ResponseBody mResponse;

    public ProcessedResponse() {}

    public static <T extends ProcessedResponse> T create(ResponseBody response, Class<T> cls) {
        if (response == null) {
            Log.w(TAG, "createFromResponseBody = got null response for class: " + cls.getName());
        }

        T responseObject;
        try {
            responseObject = cls.newInstance();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        responseObject.mResponse = response;
        return responseObject;
    }
}
