package com.example.loadgoogleimages;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import java.util.List;
import java.util.concurrent.Future;

public class NetworkManager {

    public static final String TAG = NetworkManager.class.getName();

    private static NetworkManager instance;
    private GoogleImagesApi googleImagesApi;

    private NetworkManager() {
        googleImagesApi = Network.getInstance().generateService(GoogleImagesApi.class);
    }

    public static synchronized NetworkManager getInstance() {
        if (instance == null) {
            instance = new NetworkManager();
        }

        return instance;
    }

    public interface GoogleImagesResponseCallback extends Utils.ResponseInterface<GoogleImagesResponse> {
        @Override
        void handleResponse(GoogleImagesResponse response);
    }

    @JsonIgnoreProperties
    public static class GoogleImagesResponse extends ProcessedResponse {

        @JsonProperty("images_results")
        public List<GoogleImageDataModel> imagesResults;

//        @JsonProperty("images_results")
//        public void setImagesResults(JsonNode imagesResultsMap) {
//            imagesResults = Utils.parseJsonList(imagesResultsMap.get("images_results"), new TypeReference<List<GoogleImageDataModel>>() {});
//        }
    }

    public GoogleImagesResponse getGoogleImages(String query) {
        return GoogleImagesResponse.create(Utils.executeCall(
                googleImagesApi.getGoogleImages(
                        "google", query, "google.com", 0, "isch")), GoogleImagesResponse.class);
    }

    public Future<?> getGoogleImages(String query, GoogleImagesResponseCallback cb) {
        return Network.runInThreadPool(() -> Utils.deliverResponse(cb, NetworkManager.this.getGoogleImages(query)));
    }
}
