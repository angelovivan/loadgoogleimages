package com.example.loadgoogleimages;

import android.util.Log;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;

public class Utils {
    private static final String TAG = Utils.class.getName();
    private static final int RESPONSE_CODE_OK = 200;

    private static ObjectMapper objectMapper = new ObjectMapper();

    public static <T> List<T> parseJsonList(String jsonString, TypeReference<List<T>> typeReference) {
        if(jsonString == null) {
            Log.e(TAG, "parseJsonList - jsonString is null; returning empty list");
            return new ArrayList<>();
        }

        try {
            JsonNode listJsonNode = objectMapper.readValue(jsonString, JsonNode.class);

            if (listJsonNode != null) {
                return parseJsonList(listJsonNode, typeReference);
            }
        } catch (IOException e) {
            Log.e(TAG, "parseJsonList - IOException thrown parsing JSON", e);
        }

        Log.e(TAG, "parseJsonList - got to end of method without returning properly parsed list");
        return new ArrayList<>();
    }

    public static <T> List<T> parseJsonList(JsonNode node, TypeReference<List<T>> typeReference) {
        try {
            return objectMapper.readValue(node.traverse(), typeReference);
        } catch (IOException e) {
            Log.e(TAG, "Failed to parse JSON list " + typeReference.getType(), e);
            throw new RuntimeException(e);
        }
    }

    public static <T> String toJson(T object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            Log.e(TAG, "Error serializing object to JSON", e);
            return null;
        }
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ElementType.METHOD})
    public @interface NetworkThread {}

    public interface ResponseInterface<T> {
        void handleResponse(T response);
    }

    public static <T>void deliverResponse(final ResponseInterface<T> responseInterface, final T response) {
        if (responseInterface == null) {
            return;
        }

        try {
            // Because of lambda expressions we need to lookup for the method ref that has Object as param, not the one
            // that has the concrete type. If the param isn't a lambda then both methods references exist.
            Method method = responseInterface.getClass().getMethod("handleResponse", Object.class);
            NetworkThread annotation = method.getAnnotation(NetworkThread.class);
            if (annotation == null) {
                Log.d(TAG, "Running on UI thread");
                Network.getHandler().post(() -> responseInterface.handleResponse(response));
            } else {
                Log.d(TAG, "Running off UI thread");
                responseInterface.handleResponse(response);
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public static ResponseBody executeCall(Call<ResponseBody> call) {
        try {
            retrofit2.Response<ResponseBody> response = call.execute();
            if (response.code() == RESPONSE_CODE_OK && response.body() != null) {
                return response.body();
            } else {
                Log.e(TAG, "Failed to get response for call: " + call.toString());
                return null;
            }
        } catch (Exception ex) {
            Log.e(TAG, "Exception when calling API", ex);
            return null;
        }
    }
}
