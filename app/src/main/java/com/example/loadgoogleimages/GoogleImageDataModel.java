package com.example.loadgoogleimages;

import com.google.gson.annotations.Expose;

public class GoogleImageDataModel {

    @Expose
    public int position;

    @Expose
    public String thumbnail;

    @Expose
    public String original;

    @Expose
    public String source;

    @Expose
    public String title;

    @Expose
    public String link;

    public GoogleImageDataModel() {}

    @Override
    public String toString() {
        return "GoogleImageDataModel [position=" + position +
                ", thumbnail=" + thumbnail +
                ", original=" + original +
                ", source=" + source +
                ", title=" + title +
                ", link=" + link;
    }
}
