package com.example.loadgoogleimages;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okio.BufferedSink;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface GoogleImagesApi {

//    @JsonIgnoreProperties(ignoreUnknown = true)
//    @JsonInclude(JsonInclude.Include.NON_NULL)
    class GoogleImagesRequest extends RequestBody {
        final String engine = "google";
        final String q;
        final String google_domain = "google.com";
        final int ijn = 0;
        final String tbm = "isch";

        @JsonIgnore
        private String mContent;

        @JsonIgnore
        private MediaType mediaType = MediaType.parse("application/json; charset=UTF-8");

        GoogleImagesRequest(String query) {
            q = query;
        }

        @Nullable
        @Override
        public MediaType contentType() {
            return mediaType;
        }

        @JsonIgnore
        public String getContent() {
            if (mContent == null) {
                Gson gson = new Gson();
                mContent = gson.toJson(this);
            }
            return mContent;
        }

        @Override
        public void writeTo(@NotNull BufferedSink bufferedSink) throws IOException {
            bufferedSink.write(getContent().getBytes(mediaType.charset()));
        }
    }

    @Headers({
            "Accept: application/json, text/javascript, */*; q=0.01",
            "Accept-Encoding: gzip, deflate, br",
            "Accept-Language: en-US,en;q=0.5",
    })
    @GET("/search.json")
    Call<ResponseBody> getGoogleImages(
            @Query("engine") String engine,
            @Query("q") String query,
            @Query("google_domain") String googleDomain,
            @Query("ijn") int ijn,
            @Query("tbm") String tbm);
}
