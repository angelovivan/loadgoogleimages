package com.example.loadgoogleimages;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Network {

    private static final String TAG = Network.class.getName();

    private static final int THREAD_POOL_SIZE = 3;

    private static Network sNetwork;
    private static Context sApplicationContext;
    private static Handler sHandler = new Handler(Looper.getMainLooper());
    private static Retrofit sRetrofit;
    private static OkHttpClient sOkHttpClient;

    private ScheduledThreadPoolExecutor mExecutor;

    private Network() {
        init();
    }

    public static synchronized Network getInstance() {
        if (sNetwork == null) {
            sNetwork = new Network();
        }

        return sNetwork;
    }

    private void init() {
        Log.d(TAG, "Init Network manager!");
        mExecutor = new ScheduledThreadPoolExecutor(THREAD_POOL_SIZE);

        initNetworkClients();
    }

    private static void initNetworkClients() {

        String baseUrl =  "https://serpapi.com";
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();
//        okHttpBuilder.addInterceptor(new LoggingInterceptor());
        okHttpBuilder.addInterceptor(new EncodingInterceptor());
        okHttpBuilder.addInterceptor(logging);
        sOkHttpClient = okHttpBuilder.build();

        sRetrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .callFactory(sOkHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public <T> T generateService(Class<T> clazz) {
        return sRetrofit.create(clazz);
    }

    public static Handler getHandler() {
        return sHandler;
    }

    public static Future<?> runInThreadPool(Runnable toRun) {
        return getInstance().mExecutor.submit(wrapRunnable(toRun));
    }

    private static Runnable wrapRunnable(final Runnable runnable) {
        return () -> {
            try {
                runnable.run();
            } catch (Exception ex) {
                Log.e(TAG, "Uncaught exception in a NETWORK thread!", ex);
            }
        };
    }
}
