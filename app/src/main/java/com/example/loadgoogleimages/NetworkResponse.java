package com.example.loadgoogleimages;

import android.util.Log;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.BufferedSource;
import okio.Okio;

public class NetworkResponse extends ResponseBody {
    private static final String TAG = NetworkResponse.class.getName();
    private static final int RESPONSE_CODE_OK = 200;

    public Response mResponse;
    public String responseBodyStr;

    @JsonIgnore
    private MediaType mediaType = MediaType.parse("application/json; charset=UTF-8");

    public NetworkResponse(Response response) {
        if (response != null) {
            mResponse = response;
            try {
                responseBodyStr = response.body().string();
            } catch (IOException e) {
                Log.e(TAG, "Failed to parse response body.");
                // TODO: Implement error handling
            }
        }
    }

    public NetworkResponse(String bodyString) {
        if (bodyString != null) {
            responseBodyStr = bodyString;
        }
    }


    @Override
    public long contentLength() {
        return responseBodyStr.length();
    }

    @Nullable
    @Override
    public MediaType contentType() {
        return mediaType;
    }

    @NotNull
    @Override
    public BufferedSource source() {
        return Okio.buffer(Okio.source(new ByteArrayInputStream(responseBodyStr.getBytes(mediaType.charset()))));
    }

    @Override
    public String toString() {
        return responseBodyStr;
    }

    public boolean ok() {
        return mResponse.code() == RESPONSE_CODE_OK;
    }
}
